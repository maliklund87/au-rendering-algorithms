#ifndef HASHTABLE_H_INCLUDED
#define HASHTABLE_H_INCLUDED

#include <vector>
#include <cstdlib>
#include <iostream>
#include "Vector3.h"
#include "MeasurementPoint.h"

class HashTable
{
    typedef std::vector<MeasurementPoint *> Bucket;
public:
    HashTable(int size = 10, float radius = 0, Vector3 min = Vector3(0), double hashS = 0.0f);
    ~HashTable();

    inline void addMP(int ix, int iy, int iz, MeasurementPoint* mp)
    {
	Bucket& bucket = get(ix, iy, iz);
	if (bucket.size() == 0)
	{
	    hashList[hash(ix, iy, iz)]->push_back(mp);
	}
	else
	{
	    for (size_t i = 0; i < bucket.size() ; i++)
	    {
		MeasurementPoint* existingMP = bucket[i];
		Vector3 dist = mp->pos - existingMP->pos;
		if (dot(dist, dist) < (existingMP->r2*0.66))
		{
		    for (size_t p = 0; p < mp->pixels.size(); p++)
		    {
			existingMP->pixels.push_back(mp->pixels[p]);
		    }
		}
		else
		{
		    hashList[hash(ix, iy, iz)]->push_back(mp);
		}
	    }
	}
	
    }

    virtual inline void add(MeasurementPoint* mp)
    {
	Vector3 min = ((mp->pos - m_radius) - m_min) * m_hashS;
	Vector3 max = ((mp->pos + m_radius) - m_min) * m_hashS;
	for (int iz = abs(int(min.z)); iz <= abs(int(max.z)); iz++) // HashTable version
	{
	    for (int iy = abs(int(min.y)); iy <= abs(int(max.y)); iy++)
	    {
		for (int ix = abs(int(min.x)); ix <= abs(int(max.x)); ix++)
		{
		    if (m_joinMPs)
		    {
			addMP(ix, iy, iz, mp);
		    }
		    else
		    {
			hashList[hash(ix, iy, iz)]->push_back(mp);
		    }
		}
	    }
	}
    }
    virtual inline Bucket& get(float ix, float iy, float iz)
    {
	Vector3 pos = (Vector3(ix, iy, iz) - m_min) * m_hashS;
	return *(hashList[hash(pos.x, pos.y, pos.z)]);
    }

    inline std::vector<Bucket*>& getHashList() { return hashList; }

private:
    std::vector<Bucket*> hashList;
    bool m_joinMPs;
    float m_radius;
    Vector3 m_min;
    double m_hashS;

    inline unsigned int hash(int ix, int iy, int iz)
    {
	return (unsigned int)
	    ((abs(ix)*73856093)^(abs(iy)*19349663)^((abs(iz)*83492791))) % hashList.size();
    }
};

#endif
