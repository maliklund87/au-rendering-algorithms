#include <stdlib.h>

#include "PointLight.h"
#include "Ray.h"
#include "Miro.h"

Ray
PointLight::generateRandomRay()
{
    float r1 = static_cast<float>(rand())/static_cast<float>(RAND_MAX);
    float r2 = static_cast<float>(rand())/static_cast<float>(RAND_MAX);

    double p = 2.0 * PI * r1;
    //double t = acos(1.0 - 2*r2);
    double squared = sqrt(r2*(1-r2));
    Vector3 dir = Vector3(2*cos(p)*squared, 2*sin(p)*squared, 1-2*r2);
    Ray ray(m_position, dir.normalize());
    return ray;
}
