#ifndef MEASUREMENT_POINT_H_INCLUDED
#define MEASUREMENT_POINT_H_INCLUDED

#include "Vector3.h"

class MeasurementPoint
{
public:
    Vector3 f, pos, normal, flux;
    double r2;
    unsigned int n; // n = N / ALPHA in the paper (what paper?)
    std::vector<int> pixels;
    
};

#endif
