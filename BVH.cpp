#include <algorithm>
#include <vector>
#include <stack>
#include <limits>
#include <sys/time.h>
#include "BVH.h"
#include "Ray.h"
#include "Console.h"

#define CHILDREN 100

int nrOfNodes, nrOfLeafNodes;

BVH::~BVH()
{
    // delete m_root;
}

void
BVH::calculateBoundingBox(const Objects& objects, int start, int end, Vector3& p1, Vector3& p2)
{
    Object& f = *(objects[start]);
    Vector3 minPoint = f.getMinPoint();
    Vector3 maxPoint = f.getMaxPoint();
    for (size_t i = start + 1; i < end; i++)
    {
	Object& o = *(objects[i]);
	Vector3 fMin = o.getMinPoint();
	Vector3 fMax = o.getMaxPoint();
	minPoint.x = std::min(minPoint.x, fMin.x);
	minPoint.y = std::min(minPoint.y, fMin.y);
	minPoint.z = std::min(minPoint.z, fMin.z);

	maxPoint.x = std::max(maxPoint.x, fMax.x);
	maxPoint.y = std::max(maxPoint.y, fMax.y);
	maxPoint.z = std::max(maxPoint.z, fMax.z);
    }

    p1.x = minPoint.x; p1.y = minPoint.y; p1.z = minPoint.z;
    p2.x = maxPoint.x; p2.y = maxPoint.y; p2.z = maxPoint.z;
}

float
BVH::calculateBoxVolume(const Objects& objects, int start, int end)
{
    Vector3 minPoint, maxPoint;
    calculateBoundingBox(objects, start, end, minPoint, maxPoint);
    float width = maxPoint.x - minPoint.x;
    float height = maxPoint.y - minPoint.y;
    float depth = maxPoint.z - minPoint.z;
    return width * height * depth;
}

#define SPLIT_NR 4
#define FLOAT_MIN std::numeric_limits<float>::min()
#define FLOAT_MAX std::numeric_limits<float>::max()

#define COST_OBJECT 3
#define COST_BOX 1



int
BVH::calculateBestSplit(const BVHNode& node)
{
    int sliceSize = node.end - node.start;
    if (sliceSize < CHILDREN)
	return -1;

    float costUnsplit = sliceSize * COST_OBJECT;

    // calculate split points
    int splits [SPLIT_NR - 1];
    int splitIncs = sliceSize / SPLIT_NR;
    for (int i = 1; i < SPLIT_NR; i++)
    {
	splits[i-1] = splitIncs * i;
    }

    // Find the cheapest split
    float volume_parent = node.getVolume();
    float minCost = FLOAT_MAX, splitMin;
    //std::cout << "  Testing splits: ";
    for (int i = 0; i < (SPLIT_NR - 1); i++)
    {
	int splitPoint = node.start + splits[i];
	//std::cout << splitPoint << " ";
	float leftSize = node.start - splitPoint;
	float rightSize = splitPoint - node.end;

	
	float volume_left = calculateBoxVolume(*node.m_objects, node.start, splitPoint);
	float volume_right = calculateBoxVolume(*node.m_objects, splitPoint, node.end);
	
        float costLeft = (volume_left/volume_parent) * leftSize * COST_OBJECT;
        float costRight = (volume_right/volume_parent) * rightSize * COST_OBJECT;
        float splitCost = (2 * COST_BOX) + costLeft + costRight;

	if (minCost > splitCost)
	{
	    minCost = splitCost;
	    splitMin = splitPoint;
	}
    }
    // std::cout << std::endl;
    // std::cout << "  Split: " << node.start << "|" << splitMin << "|" << node.end
    // 	      << std::endl;

    return costUnsplit < minCost? -1 : splitMin;
}


struct objectSorterStruct
{
    int sortAxis;
    bool operator()(Object* o1, Object* o2)
	{
	    Vector3 minPoint1 = o1->getMinPoint();
	    Vector3 minPoint2 = o2->getMinPoint();
	    bool result = false;
	    switch(sortAxis)
	    {
	    case 0:
		result = minPoint1.x < minPoint2.x;
		break;
	    case 1:
		result = minPoint1.y < minPoint2.y;
		break;
	    case 2:
		result = minPoint1.z < minPoint2.z;
		break;
	    default:
		result = false;
	    }
	    return result;
	}
} objectSorter;

void
BVH::build(Objects * objs)
{
    // timekeeping: start clock
    timeval tsStart, tsEnd;
    gettimeofday(&tsStart, NULL);
    
    // construct the bounding volume hierarchy
    
    m_objects = objs;

    m_root = new BVHNode(false, m_objects, 0, m_objects->size());
    Objects* rootList = m_root->m_objects;
    nrOfNodes += 1;
    std::vector<BVHNode*> nodeList;
    nodeList.push_back(m_root);
    
    bool stop = false;
    int depth = -1;
    size_t start = 0;
    size_t end = nodeList.size();
    objectSorter.sortAxis = 0;
    while (!stop)
    {
	stop = true;
	depth += 1;
	for (size_t i = start; i < end; i++)
	{
	    BVHNode* node = nodeList[i];
	    Objects* nodeObjects = node->m_objects;
	    std::sort(nodeObjects->begin() + node->start,
		      nodeObjects->begin() + node->end,
		      objectSorter);
	    
	    int nodeSize = node->end - node->start;
	    int split = calculateBestSplit(*node);
	    if (split < 0 || nodeSize < CHILDREN || depth > 9)
	    {
		node->isLeaf = true;
		nrOfLeafNodes += 1;
	    }
	    else
	    {
		stop = false;
		// create two new BVHNode
		BVHNode* left = BVHNode::createNode(nodeObjects, node->start, split);
		BVHNode* right = BVHNode::createNode(nodeObjects, split, node->end);

		node->left = left;
		node->right = right;
		
		nodeList.push_back(left);
		nodeList.push_back(right);
		nrOfNodes += 2;
	    }
	}
	start = end;
	end = nodeList.size();
	objectSorter.sortAxis = (objectSorter.sortAxis + 1)%3;
    }

    gettimeofday(&tsEnd, NULL);
    
    double elapsedTime;
    elapsedTime = (tsEnd.tv_sec - tsStart.tv_sec) * 1000.0;      // sec to ms
    elapsedTime += (tsEnd.tv_usec - tsStart.tv_usec) / 1000.0;   // us to ms

    std::cout << "BVH build time: " << elapsedTime << "ms" <<  std::endl;
    std::cout << "  Nr of nodes: " << nrOfNodes <<
	", nr of leaves: " << nrOfLeafNodes <<
	std::endl;
}

bool
BVH::intersect(HitInfo& minHit, const Ray& ray, float tMin, float tMax) const
{
    minHit.t = MIRO_TMAX;

    const BVHNode* currentNode = m_root;

    float tHit;

    //return m_root->intersect(minHit, ray, tMin, tMax);

    std::stack<const BVHNode*> s;
    while (!s.empty() || currentNode)
    {
     	if (!currentNode)
     	{
     	    // Reached the botttom, now go 'back' a step.
     	    currentNode = s.top();
     	    currentNode = currentNode->right;
     	    s.pop();
     	}
     	else
     	{
     	    if (currentNode->intersectsBoundingBox(ray, &tHit) && tHit < minHit.t)
     	    {
     		if (currentNode->isLeaf)
     		{
     		    currentNode->intersect(minHit, ray, tMin, tMax);
     		}
     		else
     		{
     		    s.push(currentNode);
     		}
     		currentNode = currentNode->left;
     	    }
     	    else
     	    {
     		currentNode = 0; // currentNode doesn't intersect, so no need to check children
     	    }
     	}
    }
    
    return minHit.t < MIRO_TMAX;
}
void
BVH::drawGL()
{
    const BVHNode* currentNode = m_root;

    std::stack<const BVHNode*> s;
    while (!s.empty() || currentNode)
    {
	if (!currentNode)
	{
	    // Reached the botttom, now go 'back' a step.
	    currentNode = s.top();
	    currentNode = currentNode->right;
	    s.pop();
	}
	else
	{
	    Vector3 pMin = currentNode->pMin;
	    Vector3 pMax = currentNode->pMax;
	    if (!currentNode->isLeaf)
	    {
		s.push(currentNode);
		currentNode = currentNode->left;
		Vector3 green = Vector3(0.0f, 1.0f, 0.0f);
		drawBox(green, pMin, pMax);
	    }
	    else
	    {
		Vector3 red = Vector3(1.0f, 0.0f, 0.0f);
	        drawBox(red, pMin, pMax);
		currentNode = 0;
	    }
	}
    }
}


void
BVH::drawBox(const Vector3& color, const Vector3& pMin, const Vector3& pMax)
{

    glColor3f(color.x, color.y, color.z);

    // front, clockwise
    glBegin(GL_QUADS);                     
    {
	glVertex3f(pMin.x, pMin.y, pMin.z);  
	glVertex3f(pMin.x, pMax.y, pMin.z);   
	glVertex3f(pMax.x, pMax.y, pMin.z);
	glVertex3f(pMax.x, pMin.y, pMin.z);
    }
    glEnd();

    // left
    glBegin(GL_QUADS);                     
    {
	glVertex3f(pMin.x, pMin.y, pMin.z);  
	glVertex3f(pMin.x, pMax.y, pMin.z);   
	glVertex3f(pMin.x, pMax.y, pMax.z);
	glVertex3f(pMin.x, pMin.y, pMax.z);
    }
    glEnd();

    // right
    glBegin(GL_QUADS);                     
    {
	glVertex3f(pMax.x, pMin.y, pMin.z);  
	glVertex3f(pMax.x, pMax.y, pMin.z);   
	glVertex3f(pMax.x, pMax.y, pMax.z);
	glVertex3f(pMax.x, pMin.y, pMax.z);
    }
    glEnd();
		
    // right
    glBegin(GL_QUADS);                     
    {
	glVertex3f(pMin.x, pMin.y, pMax.z);  
	glVertex3f(pMin.x, pMax.y, pMax.z);   
	glVertex3f(pMax.x, pMax.y, pMax.z);
	glVertex3f(pMax.x, pMin.y, pMax.z);
    }
    glEnd();
		
    glColor3f(1.0f, 1.0f, 1.0f);
}
