#include "HashTable.h"
#include "MeasurementPoint.h"

HashTable::HashTable(int size, float radius, Vector3 min, double hashS):
    m_radius(radius),
    m_min(min),
    m_hashS(hashS),
    m_joinMPs(false)
{
    for (size_t i = 0; i < size; i++)
    {
	hashList.push_back(new Bucket());
    }
}

HashTable::~HashTable()
{
    for (size_t i = 0; i < hashList.size(); i++)
    {
	delete hashList[i];
    }
}
