#include "Ray.h"

#include <utility>

void
Ray::swap(Ray& other)
{
    std::swap(refIndexStack, other.refIndexStack);
    o = other.o;
    d = other.d;
    invD = other.invD;
    depth = other.depth;
}
