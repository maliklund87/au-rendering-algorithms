#include "Material.h"

Material::Material()
{
}

Material::~Material()
{
}

Vector3
Material::shade(const Ray&, const HitInfo&, const Scene&) const
{
    return Vector3(1.0f, 1.0f, 1.0f);
}

void
Material::photonShade(HashTable& mpTable, Ray& ray, Scene& scene,
		      HitInfo& hit, Vector3& flux, Vector3& adj) const
{

}
