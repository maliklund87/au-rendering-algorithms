#ifndef PFMIMAGE_H_INCLUDED
#define PFMiMAGE_H_INCLUDED

#include <string>

class Vector3;

class PFMImage
{
public:
    PFMImage();
    PFMImage(const std::string& path);
    ~PFMImage();

    Vector3 get(float x, float y);
    
    inline int getWidth() { return width; }
    inline int getHeight() { return height; }

private:
    int width, height;
    Vector3* image;
};


#endif // PFMiMAGE_H_INCLUDED
