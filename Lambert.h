#ifndef CSE168_LAMBERT_H_INCLUDED
#define CSE168_LAMBERT_H_INCLUDED

#include "Material.h"

class MeasurementPoint;
class HashTable;

class Lambert : public Material
{
public:
    Lambert(const Vector3 & kd = Vector3(1),
            const Vector3 & ka = Vector3(0));
    virtual ~Lambert();

    const Vector3 & kd() const {return m_kd;}
    const Vector3 & ka() const {return m_ka;}

    void setKd(const Vector3 & kd) {m_kd = kd;}
    void setKa(const Vector3 & ka) {m_ka = ka;}

    virtual void preCalc() {}
    
    virtual Vector3 shade(const Ray& ray, const HitInfo& hit,
                          const Scene& scene) const;
    
    virtual void photonShade(HashTable& mpTable, Ray& ray, Scene& scene,
			     HitInfo& hit, Vector3& flux, Vector3& adj) const;
    
    void trace(std::vector<MeasurementPoint*>& mPoints,
	       const Ray& ray, HitInfo& hit,
	       const Scene& scene,
	       const Vector3& adj,
	       float tMin = 0.0f, float tMax = MIRO_TMAX) const;
    
protected:
    Vector3 m_kd;
    Vector3 m_ka;

    Ray generateDiffuseRay(const HitInfo& hit) const;
};

#endif // CSE168_LAMBERT_H_INCLUDED
