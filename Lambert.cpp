#include "Lambert.h"
#include "Ray.h"
#include "Scene.h"
#include "MeasurementPoint.h"
#include "HashTable.h"

#define ALPHA 0.7f

Lambert::Lambert(const Vector3 & kd, const Vector3 & ka) :
    m_kd(kd), m_ka(ka)
{

}

Lambert::~Lambert()
{
}

void
Lambert::trace(std::vector<MeasurementPoint*>& mPoints,
	       const Ray& ray, HitInfo& hit,
	       const Scene& scene,
	       const Vector3& adj,
	       float tMin, float tMax) const
{
    MeasurementPoint* mp = new MeasurementPoint;
    mp->pos = hit.P;
    mp->normal = hit.N;
    mp->f = m_kd*adj;
    mPoints.push_back(mp);
}

void
Lambert::photonShade(HashTable& mpTable, Ray& ray,  Scene& scene,
		     HitInfo& hit, Vector3& flux, Vector3& adj) const
{
    if (ray.depth > 20 || flux.length() < 0.001f)
    {
	return;
    }
    
    Vector3& hitPos = hit.P;
    std::vector<MeasurementPoint*>& bucket = mpTable.get(hitPos.x, hitPos.y, hitPos.z);
    for (size_t i = 0; i < bucket.size(); i++)
    {
	MeasurementPoint* mp = bucket[i];
	
	Vector3 dist = mp->pos - hitPos;
	if (dot(mp->normal, hit.N) > 0.001f && (dot(dist, dist) <= mp->r2))
	{
	    double g = (mp->n*ALPHA+ALPHA) / (mp->n*ALPHA + 1.0);
	    mp->r2 = mp->r2*g;
	    mp->n++;
	    mp->flux += (mp->f * flux * (1.0f / PI))*g;
	}
    }

    float p = std::max(m_kd.x, std::max(m_kd.y, m_kd.z)); // what is this?

    Ray diffRay = generateDiffuseRay(hit);
    diffRay.depth = ray.depth + 1;
    Vector3 newFlux = m_kd*flux*(1.0f/p);
    scene.tracePhotonRay(diffRay, newFlux, adj, 0.0024f);
}

Ray
Lambert::generateDiffuseRay(const HitInfo& hit) const
{
    float r1 = static_cast<float>(rand())/static_cast<float>(RAND_MAX);
    r1 = 2.0f * PI * r1;
    float r2 = static_cast<float>(rand())/static_cast<float>(RAND_MAX);
    float r2s = sqrt(r2);

    Vector3 w = hit.N;
    Vector3 u = (cross((abs(w.x) > 0.1f ?
			Vector3(0.0f, 1.0f, 0.0f) :
			Vector3(1.0f, 0.0f, 0.0f))
		       , w)).normalize();
    Vector3 v = cross(w, u);
    Vector3 d = (u*cos(r1)*r2s + v*sin(r1)*r2s + w*sqrt(1 - r2));
    d.normalize();
    Ray diffRay = Ray(hit.P, d);
    return diffRay;
}

Vector3
Lambert::shade(const Ray& ray, const HitInfo& hit, const Scene& scene) const
{
    Vector3 L = Vector3(0.0f, 0.0f, 0.0f);
    
    const Vector3 viewDir = -ray.d; // d is a unit vector
    
    const Lights *lightlist = scene.lights();
    
    // loop over all of the lights
    Lights::const_iterator lightIter;
    for (lightIter = lightlist->begin(); lightIter != lightlist->end(); lightIter++)
    {
        PointLight* pLight = *lightIter;
    
        Vector3 l = pLight->position() - hit.P;

	Ray shadowRay(hit.P, l.normalized());
	HitInfo shadowHit;
	if (!scene.trace(shadowHit, shadowRay, 0.0024f, l.length()))
	{
	    // the inverse-squared falloff
	    float falloff = l.length2();
        
	    // normalize the light direction
	    l /= sqrt(falloff);

	    // get the diffuse component
	    float nDotL = dot(hit.N, l);
	    Vector3 result = pLight->color();
	    result *= m_kd;
        
	    L += std::max(0.0f, nDotL/falloff * pLight->wattage() / PI) * result;    
	}
	else
	{
	    // remove this section  
	}
        
    }
    
    // add the ambient component
    L += m_ka;
    
    return L;
}
