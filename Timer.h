#ifndef TIMER_H_INCLUDED
#define TIMER_H_INCLUDED

#include <sys/time.h>

class Timer
{
public:

    void start();
    void stop();

    double getElapsedTime();

private:

timeval _start, _end;
    
};

#endif
