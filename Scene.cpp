
#include <cmath>
#include <cfloat>
#include "Miro.h"
#include "Scene.h"
#include "Camera.h"
#include "Image.h"
#include "Console.h"
#include "Sphere.h"
#include "MeasurementPoint.h"
#include "HashTable.h"
#include "MPTree.h"
#include "Timer.h"

Scene * g_scene = 0;
long nrOfRays;
extern long nrOfTriangleIntersections;
extern long nrOfRayTraversals;

#define ALPHA 0.7

Scene::Scene():
    m_envMapEnabled(false),
    antiAliasingEnabled(false),
    nrOfMPoints(10000),
    nrOfPhotons(4000),
    raysPrPixel(1)
{

}

Scene::~Scene()
{
    delete m_envMap;
}

void
Scene::openGL(Camera *cam)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    cam->drawGL();

    m_bvh.drawGL();

    // draw objects
    for (size_t i = 0; i < m_objects.size(); ++i)
        m_objects[i]->renderGL();
    
    glutSwapBuffers();
}

void
Scene::preCalc()
{
    Objects::iterator it;
    for (it = m_objects.begin(); it != m_objects.end(); it++)
    {
        Object* pObject = *it;
        pObject->preCalc();
    }
    Lights::iterator lit;
    for (lit = m_lights.begin(); lit != m_lights.end(); lit++)
    {
        PointLight* pLight = *lit;
        pLight->preCalc();
    }

    m_bvh.build(&m_objects);
}

void
Scene::traceEyeRay(std::vector<MeasurementPoint*>& mPoints, const Ray& ray,
		   const Vector3& adj,
		   float tMin, float tMax) const
{
    HitInfo* firstHit = new HitInfo;
    
    if (trace(*firstHit, ray, tMin, tMax))
    {
	firstHit->material->trace(mPoints, ray, *firstHit, *this, adj);
    }
    else
    {
	delete firstHit;
    }
}

void
Scene::mapMeasurementPoints()
{
    Image& image = *g_image;
    Camera& cam = *g_camera;

    std::vector<float> antiAliasPoints;
    if (antiAliasingEnabled)
    {
	antiAliasPoints.push_back(-0.25f);
	antiAliasPoints.push_back(0.25f);
	
	antiAliasPoints.push_back(0.25f);
	antiAliasPoints.push_back(0.25f);
	
	antiAliasPoints.push_back(0.25f);
	antiAliasPoints.push_back(-0.25f);
	
	antiAliasPoints.push_back(-0.25f);
	antiAliasPoints.push_back(0.25f);
    }
    else
    {
	antiAliasPoints.push_back(0.0f);
	antiAliasPoints.push_back(0.0f);
    }

    std::cout << "Tracing eye rays" << std::endl;

    // trace eye rays and store measurement points
    int pixels = image.height() * image.width();
    for (int y = 0; y < image.height(); y++)
    {
	for (int x = 0; x < image.width(); x++)
	{
	    double p = ((x + y*image.height())*100.0f)/pixels;
	    fprintf(stderr, "\rEye rays %5.2f%%", p);	    
	    for (size_t ap = 0; ap < antiAliasPoints.size(); ap += 2)
	    {
		float apX = antiAliasPoints[ap];
		float apY = antiAliasPoints[ap+1];
		Ray ray = cam.eyeRay(x, y, image.width(), image.height(), apX, apY);
		std::vector<MeasurementPoint*> tempPoints;
		traceEyeRay(tempPoints, ray, Vector3(1.0f));
		for (size_t i = 0; i < tempPoints.size(); i++)
		{
		    // set image coordinate for image points.
		    MeasurementPoint* mp = tempPoints[i];
		    mp->pixels.push_back(x + y*image.width());
		    mPoints.push_back(mp);
		}
	    }
	    
	}
    }

    std::cout << std::endl << "Building hash grid" << std::endl;

    // build hash table
    buildHashGrid(image.width(), image.height());

    std::cout << "Tracing photon rays" << std::endl;
    // trace photon rays
    tracePhotonRays();
}

void
Scene::tracePhotonRay(Ray& ray, Vector3& flux, Vector3& adj, float tMin, float tMax)
{
    HitInfo hit;
    if (trace(hit, ray, tMin, tMax))
    {
	hit.material->photonShade(*mpHashTable, ray, *this, hit, flux, adj);
    }	    
}

void
Scene::tracePhotonRays()
{
    int totalRepeats = (nrOfPhotons/(m_lights.size()*raysPrPixel));
    int count = 0;
    for (int i = 0; i < totalRepeats; i++)
    {
	double p = 100.0 * (i+1)/totalRepeats; // percentage done
	fprintf(stderr, "\rPhotonPass %5.2f%%", p);

	for (size_t il = 0; il < m_lights.size(); il++)
	{
	    PointLight* l = m_lights[il];
	    Ray pRay = l->generateRandomRay();
	    Vector3 flux = Vector3(l->wattage());
	    Vector3 adj = l->color();
	    for (int r = 0; r < raysPrPixel; r++)
	    {
		tracePhotonRay(pRay, flux, adj);
		count++;
	    }
	}
    }
    std::cout << std::endl << "Total photons: " << count << std::endl;
    fprintf(stderr, "\n");
}

void
Scene::buildHashGrid(int width, int height)
{
    Vector3 bbMin(FLT_MAX);
    Vector3 bbMax(FLT_MIN);
    for (size_t i = 0; i < mPoints.size(); i++)
    {
	MeasurementPoint* mp = mPoints[i];
	adjustBoundingBox(mp->pos, &bbMin, &bbMax);
    }

    // heuristic for initial radius
    Vector3 sSize = bbMax - bbMin;
    std::cout << "MeasurementPoint BB size: " << sSize << std::endl;
    
    double initRadius = ((sSize.x + sSize.y + sSize.z) / 3.0) / ((width + height) / 2.0) * 3.0f;
    std::cout << "  MP initial radius: " << initRadius << std::endl;

    // determine hash table size
    bbMin = Vector3(FLT_MAX);
    bbMax = Vector3(FLT_MIN);
    int vPhoton = 0;
    std::cout << "  Nr of mps: " << mPoints.size() << std::endl;
    for (size_t i = 0; i < mPoints.size(); i++)
    {
	MeasurementPoint* mp = mPoints[i];
	mp->r2 = initRadius * initRadius;
	mp->n = 0;
	mp->flux = Vector3();
	vPhoton++;

	adjustBoundingBox(mp->pos - initRadius, &bbMin, &bbMax);
	adjustBoundingBox(mp->pos + initRadius, &bbMin, &bbMax);
    }


    // make each grid cell two times larger than the initial radius
    double hashS = 1.0/(initRadius * 2.0);
    mpHashTable = new HashTable(vPhoton, initRadius, bbMin, hashS);
    //mpHashTable = new MPTree(bbMin, bbMax, initRadius, 20);
    for (size_t i = 0; i < mPoints.size(); i++)
    {
	mpHashTable->add(mPoints[i]); // MPTree version
    }

    std::vector<std::vector<MeasurementPoint*>* >& bucketList = mpHashTable->getHashList();
    float avgSize = 0;
    int zeros = 0;
    int smallest = 4000000;
    int largest = 0;
    for (size_t i = 0; i < bucketList.size(); i++)
    {
	if (bucketList[i]->size() > 0)
	{
	    avgSize += bucketList[i]->size();
	    smallest = std::min((int)bucketList[i]->size(), smallest);
	    largest = std::max((int)bucketList[i]->size(), largest);
	}
	
	if (bucketList[i]->size() == 0)
	    zeros++;

    }
    std::cout << "HashTable, avg bucket size: " << avgSize/(bucketList.size() - zeros) << std::endl;
    std::cout << "Empty buckets: " << zeros << std::endl;
    std::cout << "Smallest bucket: " << smallest << std::endl;
    std::cout << "Largest bucket: " << largest << std::endl;
}

void
Scene::adjustBoundingBox(const Vector3& pos, Vector3* bbMin, Vector3* bbMax)
{
    bbMin->x = std::min(pos.x, bbMin->x);
    bbMin->y = std::min(pos.y, bbMin->y);
    bbMin->z = std::min(pos.z, bbMin->z);

    bbMax->x = std::max(pos.x, bbMax->x);
    bbMax->y = std::max(pos.y, bbMax->y);
    bbMax->z = std::max(pos.z, bbMax->z);
}

void
Scene::raytraceImage(Camera *cam, Image *img)
{
    
    Ray ray;
    HitInfo hitInfo;
    Vector3 shadeResult;

    Timer timer;
    timer.start();
    // timeval tsStart, tsEnd;
    // gettimeofday(&tsStart, NULL);
    
    // trace eye rays
    mapMeasurementPoints();

    bool photonShade = true;
    if(photonShade)
    {
	Vector3* vImg = new Vector3[img->width()*img->height()];
	for (size_t i = 0; i < mPoints.size(); i++)
	{
	    MeasurementPoint* mp = mPoints[i];

	    // Vector3 shadeResult = mp->f;

	    // Vector3& pixel = vImg[mp->pix];

	    int aaConstant = 1;
	    if (antiAliasingEnabled)
		aaConstant = 4;
	    for (size_t p = 0; p < mp->pixels.size(); p++)
	    {
		vImg[mp->pixels[p]] += mp->flux*(1.0f/(PI*mp->r2*nrOfPhotons*aaConstant));
	    }
	}

	for (int j = 0; j < img->height(); j++)
	{
	    for (int i = 0; i < img->width(); i++)
	    {
		img->setPixel(i, j, vImg[i + j*img->width()]);
	    }
	}
	
	// delete vImg array
	delete[] vImg;
    }
    else
    {	
	//loop over all pixels in the image
	for (int j = 0; j < img->height(); ++j)
	{
	    for (int i = 0; i < img->width(); ++i)
	    {
		// simulate thin lens camera here
		ray = cam->eyeRay(i, j, img->width(), img->height());
		Vector3 shadeResult;
		if (cam->isThinLensEnabled())
		{
		    Vector3 focalPoint = cam->getFocalPoint(ray);

		    int raysPrPixel = 32;
		    for (int r = 0; r < raysPrPixel; r++)
		    {
			nrOfRays += 1;
			Ray lensRay = cam->randomRay(focalPoint);
			if (trace(hitInfo, lensRay))
			{
			    shadeResult += hitInfo.material->shade(lensRay, hitInfo, *this);
			}
			else if (isEnvMapEnabled())
			{
			    shadeResult += getEnvMapColor(lensRay.d);
			}
		    }
		    shadeResult = shadeResult / raysPrPixel;
		}
		else
		{
		    if (trace(hitInfo, ray))
		    {
			nrOfRays += 1;
			shadeResult = hitInfo.material->shade(ray, hitInfo, *this);
		    }
		    else if (isEnvMapEnabled())
		    {
			shadeResult = getEnvMapColor(ray.d);
		    }
		}

		img->setPixel(i, j, shadeResult);
	    }
	    img->drawScanline(j);
	    glFinish();
	    printf("Rendering Progress: %.3f%%\r", j/float(img->height())*100.0f);
	    fflush(stdout);
	}
    }
    
    // gettimeofday(&tsEnd, NULL);
    timer.stop();
    
    printf("Rendering Progress: 100.000%%\n");

    // render time
    double elapsedTime = timer.getElapsedTime();
    // elapsedTime = (tsEnd.tv_sec - tsStart.tv_sec) * 1000.0;      // sec to ms
    // elapsedTime += (tsEnd.tv_usec - tsStart.tv_usec) / 1000.0;   // us to ms
    std::cout << "  Render time: " << 
	elapsedTime << "ms" <<  
	std::endl;

    // Nr of rays
    std::cout << "  Nr of rays: " << nrOfRays << std::endl;

    // Nr of ray traversals
    std::cout << "  Nr of ray traversals: " << nrOfRayTraversals << std::endl;

    // nr of ray triangle intersections
    std::cout << "  Nr of ray-triangle intersections: " <<
	nrOfTriangleIntersections << std::endl;
    
    debug("done Raytracing!\n");
    
}

Vector3
Scene::getEnvMapColor(const Vector3& dir)
{   
    // Prevent division by 0
    if (dir.x == 0.0f && dir.y == 0.0f)
	return Vector3();

    float root = sqrt(dir.x*dir.x + dir.y*dir.y);
    float r = (1/PI) * (acos(dir.z) / root );

    float u = dir.x * r;
    float v = dir.y * r;
    
    return m_envMap->get(u, v);
}

bool
Scene::trace(HitInfo& minHit, const Ray& ray, float tMin, float tMax) const
{
    return m_bvh.intersect(minHit, ray, tMin, tMax);
}

void
Scene::updateImage(Image& img, int j)
{
        img.drawScanline(j);
        glFinish();
        printf("Rendering Progress: %.3f%%\r", j/float(img.height())*100.0f);
        fflush(stdout);
}
