#ifndef CSE168_RAY_H_INCLUDED
#define CSE168_RAY_H_INCLUDED

#include <stack>
#include "Vector3.h"

class Ray
{
public:
    Vector3 o,  //!< Origin of ray
	d,      //!< Direction of ray
	invD;   //!< Used for ray-box intersection

    std::stack<float> refIndexStack;
    int depth;
    

Ray() :
    o(), 
	d(Vector3(0.0f,0.0f,1.0f)),
	depth(0)
    {
        computeInvDir();
	push(1.0f);
    }

Ray(const Vector3& o, const Vector3& d) : o(o), d(d), depth(0)
    {
        computeInvDir();
	push(1.0f);
    }

    inline void push(float i)
    {
	refIndexStack.push(i);
    }

    inline float head() const
    {
	return refIndexStack.top();
    }

    inline void pop()
    {
	refIndexStack.pop();
    }

    void swap(Ray& other);

    Ray& operator=(Ray other)
    {
	swap(other);
	return *this;
    }
    
private:
    void computeInvDir()
    {
	invD = Vector3(1/d.x, 1/d.y, 1/d.z);
    }
    
};

class Material;

//! Contains information about a ray hit with a surface.
/*!
    HitInfos are used by object intersection routines. They are useful in
    order to return more than just the hit distance.
*/
class HitInfo
{
public:
    float t;                            //!< The hit distance
    Vector3 P;                          //!< The hit point
    Vector3 N;                          //!< Shading normal vector
    const Material* material;           //!< Material of the intersected object

    //! Default constructor.
    explicit HitInfo(float t = 0.0f,
                     const Vector3& P = Vector3(),
                     const Vector3& N = Vector3(0.0f, 1.0f, 0.0f)) :
    t(t), P(P), N(N), material (0)
    {
        // empty
    }
    
    inline static void copy(HitInfo& source, HitInfo& target)
    {
	target.t = source.t;
	target.P = source.P;
	target.N = source.N;
	target.material = source.material;
    }
};

#endif // CSE168_RAY_H_INCLUDED
