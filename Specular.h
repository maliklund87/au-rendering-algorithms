#ifndef SPECULAR_H_INCLUDED
#define SPECULAR_H_INCLUDED

#include "Material.h"

class MeasurementPoint;

class Specular: public Material
{
public:
    Specular(const Vector3& ks = Vector3(1.0f));
    ~Specular();


    const Vector3& ks() const { return m_ks; }
    
    void setKs(const Vector3& ks) { m_ks = ks; }
    
    void preCalc();
    
    Vector3 shade(const Ray& ray, const HitInfo& hit,
		  const Scene& scene) const;
    virtual void photonShade(HashTable& mpTable, Ray& ray, Scene& scene,
			     HitInfo& hit, Vector3& flux, Vector3& adj) const;

    void trace(std::vector<MeasurementPoint*>& mPoints,
	       const Ray& ray, HitInfo& hit,
	       const Scene& scene,
	       const Vector3& adj,
	       float tMin = 0.0f, float tMax = MIRO_TMAX) const;
    

private:
    Vector3 m_ks;

    inline Ray getReflectedRay(const Ray& ray, const HitInfo& hit) const;
    
};

#endif // SPECULAR_H_INCLUDED
