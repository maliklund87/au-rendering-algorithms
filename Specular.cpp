#include "Specular.h"
#include "Ray.h"
#include "Scene.h"
#include "HashTable.h"

Specular::Specular(const Vector3& ks):
    m_ks(ks)
{

}

Specular::~Specular()
{

}

void
Specular::preCalc()
{
    // Nothing to do for now
}

Ray
Specular::getReflectedRay(const Ray& ray, const HitInfo& hit) const
{
    Vector3 reflectedDir = -2 * dot(ray.d, hit.N) * hit.N + ray.d;
    reflectedDir.normalize();
    Ray reflectedRay(hit.P, reflectedDir);
    reflectedRay.depth = ray.depth + 1;
    return reflectedRay;
}

void
Specular::trace(std::vector<MeasurementPoint*>& mPoints,
		const Ray& ray, HitInfo& hit,
		const Scene& scene,
		const Vector3& adj,
		float tMin, float tMax) const
{
    if (ray.depth > 20)
	return;

    Ray reflectedRay = getReflectedRay(ray, hit);
    scene.traceEyeRay(mPoints, reflectedRay, m_ks*adj, 0.0024f);
}

Vector3
Specular::shade(const Ray& ray, const HitInfo& hit,
		const Scene& scene) const
{
    if (ray.depth > 10)
	return Vector3(m_ks);
    
    Ray reflectedRay = getReflectedRay(ray, hit);

    HitInfo reflectedHit;
    Vector3 result;
    if (scene.trace(reflectedHit, reflectedRay, 0.00024f, MIRO_TMAX))
    {
	result = reflectedHit.material->shade(reflectedRay, reflectedHit, scene);
	result = m_ks * result;
    }
    
    return result;
}

void
Specular::photonShade(HashTable& mpTable, Ray& ray, Scene& scene,
		      HitInfo& hit, Vector3& flux, Vector3& adj) const
{
    if (ray.depth > 20 || flux.length() < 0.001f)
	return;

    Ray reflectedRay = getReflectedRay(ray, hit);
    Vector3 newFlux= m_ks*flux;
    Vector3 newAdj = m_ks*adj;
    scene.tracePhotonRay(reflectedRay, newFlux, newAdj, 0.0024f);
}
