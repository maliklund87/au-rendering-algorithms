#include "BVHNode.h"

#define EPSILON 0.00000024f

long nrOfTriangleIntersections;
long nrOfRayTraversals;

BVHNode::BVHNode(bool isLeaf, Objects* objects, int start, int end)
{
    this->isLeaf = isLeaf;
    this->start = start;
    this->end = end;
    this->m_objects = objects;

    left = 0; right = 0;

    BVHNode::setBoundingBox(*m_objects, start, end, pMin, pMax);
}

BVHNode::~BVHNode()
{
    delete left;
    delete right;
    delete m_objects;
}

BVHNode*
BVHNode::createNode(Objects* objects, int start, int end)
{
    BVHNode* node = new BVHNode(false, objects, start, end);
    return node;
}

BVHNode*
BVHNode::createLeaf(Objects* objects, int start, int end)
{
    BVHNode* node = new BVHNode(true, objects, start, end);
    return node;
}

bool
BVHNode::intersectsBoundingBox(const Ray& ray, float* t) const
{
    // Slab test
    float t1x, t1y, t1z, t2x, t2y, t2z;
    t1x = (pMin.x - ray.o.x) * ray.invD.x;
    t1y = (pMin.y - ray.o.y) * ray.invD.y;
    t1z = (pMin.z - ray.o.z) * ray.invD.z;
    t2x = (pMax.x - ray.o.x) * ray.invD.x;
    t2y = (pMax.y - ray.o.y) * ray.invD.y;
    t2z = (pMax.z - ray.o.z) * ray.invD.z;

    float tMin = std::max(std::min(t1x, t2x), std::max(std::min(t1y, t2y), std::min(t1z, t2z)));
    float tMax = std::min(std::max(t1x, t2x), std::min(std::max(t1y, t2y), std::max(t1z, t2z)));
    tMax = tMax * 1.00000024f; // MaxMult; add 4 ulps. Robust BVH ray traversal

    if (tMin < tMax && (tMin > 0 || tMax > 0))
    {
	if (tMin > 0)
	    *t = tMin;
	else
	    *t = tMax;
	return true;
    }
    return false;
}

bool
BVHNode::intersect(HitInfo& result, const Ray& ray,
		   float tMin, float tMax) const
{
    nrOfRayTraversals += 1;
    bool hit = false;
    HitInfo tempMinHit;
    
    for (size_t i = start; i < end; ++i)
    {
	if ((*m_objects)[i]->intersect(tempMinHit, ray, tMin, tMax))
	{
	    if (tempMinHit.t < result.t) {
		hit = true;
		HitInfo::copy(tempMinHit, result);
	    }
	}
	nrOfTriangleIntersections += 1;
    }
    
    return hit;    
}

float
BVHNode::getVolume() const
{
    float width = pMax.x - pMin.x;
    float height = pMax.y - pMin.y;
    float depth = pMax.z - pMin.z;
    return width * height * depth;
}

void
BVHNode::setBoundingBox(const Objects& objects, int start, int end,
			Vector3& pMin, Vector3& pMax)
{
    Object& f = *(objects[start]);
    Vector3 minPoint = f.getMinPoint();
    Vector3 maxPoint = f.getMaxPoint();
    for (size_t i = start; i < end; i++)
    {
	Object& o = *(objects[i]);
	Vector3 fMin = o.getMinPoint();
	Vector3 fMax = o.getMaxPoint();
	minPoint.x = fMin.x < minPoint.x ? fMin.x : minPoint.x;
	minPoint.y = fMin.y < minPoint.y ? fMin.y : minPoint.y;
	minPoint.z = fMin.z < minPoint.z ? fMin.z : minPoint.z;

	maxPoint.x = std::max(maxPoint.x, fMax.x);
	maxPoint.y = std::max(maxPoint.y, fMax.y);
	maxPoint.z = std::max(maxPoint.z, fMax.z);
    }

    pMin.x = minPoint.x; pMin.y = minPoint.y; pMin.z = minPoint.z;
    pMax.x = maxPoint.x; pMax.y = maxPoint.y; pMax.z = maxPoint.z;
}

