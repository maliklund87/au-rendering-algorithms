#ifndef BVHNODE_H
#define BVHNODE_H

#include "Ray.h"
#include "Miro.h"
#include "Object.h"

class BVHNode
{
public:
    BVHNode() { };
    BVHNode(bool isLeaf, Objects* objects, int start, int end);
    ~BVHNode();
    
    bool isLeaf;
    Objects* m_objects;
    int start, end;
    Vector3 pMin, pMax;
    BVHNode* left, * right;
    
    bool intersectsBoundingBox(const Ray& ray, float* t) const;

    bool intersect(HitInfo& result, const Ray& ray,
		   float tMin = 0.0f, float tMax = MIRO_TMAX) const;

    float getVolume() const;

    static BVHNode* createNode(Objects* objects, int start, int end);
    static BVHNode* createLeaf(Objects* objects, int start, int end);

    static void setBoundingBox(const Objects& objects, int start, int end,
			       Vector3& pMin, Vector3& pMax);
private:
};

#endif // BVHNODE_H
