#ifndef REFRACT_H_INCLUDED
#define REFRACT_H_INCLUDED

#include "Material.h"

class Refract : public Material
{
public:
    Refract(float refractionIndex = 1.0f, const Vector3& ks = Vector3(1.0f));

    void preCalc();

    Vector3 shade(const Ray& ray, const HitInfo& hit,
		  const Scene& scene) const;
    virtual void photonShade(HashTable& mpTable, Ray& ray, Scene& scene,
			     HitInfo& hit, Vector3& flux, Vector3& adj) const;

    void trace(std::vector<MeasurementPoint*>& mPoints,
	       const Ray& ray, HitInfo& hit,
	       const Scene& scene,
	       const Vector3& adj,
	       float tMin = 0.0f, float tMax = MIRO_TMAX) const;
private:
    float m_index;
    Vector3 m_ks;

    bool computeNormal(const Ray& ray, const HitInfo& hit, Vector3* normal) const;
    float computeSqrtPart(float indexRatio, float rayNormalDot) const;
    float computeFresnelTerm(float n1, float n2,
			     const Vector3& normal, const Vector3& wi, const Vector3& wt) const;

    Ray getRefractedRay(const Ray& ray, const HitInfo& hit, const Vector3& normal, float indexRatio,
			float rayNormalDot, float sqrtPart) const;
    Ray getReflectedRay(const Ray& ray, const HitInfo& hit) const;
};

#endif // REFRACT_H_INCLUDED
