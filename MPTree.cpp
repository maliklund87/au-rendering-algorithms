#include "MPTree.h"

#include <cmath>

MPTree::MPTree(const Vector3& minPoint, const Vector3& maxPoint, float point, int childSize):
    size(childSize),
    size2(size*size),
    size3(size*size2),
    pointRadius(point),
    nrOfElements(size3),
    offset(Vector3(std::abs(minPoint.x), std::abs(minPoint.y), std::abs(minPoint.z))),
    inc((maxPoint - minPoint))
{
    min = minPoint + offset;
    max = maxPoint + offset;
    incDiv = Vector3(1/inc.x, 1/inc.y, 1/inc.z);
    inc = Vector3(1.0f/size);
    constant = (offset - min)*incDiv;

    mpLists.reserve(nrOfElements);
    for (int i = 0; i < nrOfElements; i++)
    {
	mpLists.push_back(new Bucket());
    }
}

MPTree::~MPTree()
{
    for (size_t i = 0; i < mpLists.size(); i++)
    {
	delete mpLists[i];
    }
}
