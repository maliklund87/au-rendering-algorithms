#include "PFMImage.h"
#include "PFMLoader.h"
#include "Vector3.h"

PFMImage::PFMImage()
{
    image = new Vector3[0];
    width = 0;
    height = 0;
}

PFMImage::PFMImage(const std::string& path)
{
    image = readPFMImage(path.c_str(), &width, &height);
}

PFMImage::~PFMImage()
{
    delete[] image;
}

Vector3
PFMImage::get(float u, float v)
{   
    if (width == 0 || height == 0 || u < -1.0f || u > 1.0f || v < -1.0f || v > 1.0f)
	return Vector3();

    int x = (u + 1)*0.5f*width;
    int y = (v + 1)*0.5f*height;

    return image[y*width + x];
}
