#ifndef CSE168_MATERIAL_H_INCLUDED
#define CSE168_MATERIAL_H_INCLUDED

#include <vector>

#include "Miro.h"
#include "Vector3.h"

class MeasurementPoint;
class HashTable;

class Material
{
public:
    Material();
    virtual ~Material();

    virtual void preCalc() {}
    
    virtual Vector3 shade(const Ray& ray, const HitInfo& hit,
                          const Scene& scene) const;
    
    virtual void photonShade(HashTable& mpTable, Ray& ray, Scene& scene,
			     HitInfo& hit, Vector3& flux, Vector3& adj) const;
    
    virtual void trace(std::vector<MeasurementPoint*>& mPoints,
		       const Ray& ray, HitInfo& hit,
		       const Scene& scene,
		       const Vector3& adj,
		       float tMin = 0.0f, float tMax = MIRO_TMAX) const = 0;
};

#endif // CSE168_MATERIAL_H_INCLUDED
