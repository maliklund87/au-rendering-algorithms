#include "Triangle.h"
#include "TriangleMesh.h"
#include "Ray.h"


Triangle::Triangle(TriangleMesh * m, unsigned int i) :
    m_mesh(m), m_index(i)
{

}


Triangle::~Triangle()
{

}


void
Triangle::renderGL()
{
    TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
    const Vector3 & v0 = m_mesh->vertices()[ti3.x]; //vertex a of triangle
    const Vector3 & v1 = m_mesh->vertices()[ti3.y]; //vertex b of triangle
    const Vector3 & v2 = m_mesh->vertices()[ti3.z]; //vertex c of triangle

    glBegin(GL_TRIANGLES);
        glVertex3f(v0.x, v0.y, v0.z);
        glVertex3f(v1.x, v1.y, v1.z);
        glVertex3f(v2.x, v2.y, v2.z);
    glEnd();
}

Vector3
Triangle::getMinPoint()
{
    TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
    
    const Vector3 & v0 = m_mesh->vertices()[ti3.x]; // vertex a of triangle
    const Vector3 & v1 = m_mesh->vertices()[ti3.y]; // vertex b of triangle
    const Vector3 & v2 = m_mesh->vertices()[ti3.z]; // vertex c of triangle

    float minX = 0, minY = 0, minZ = 0;

    minX = std::min(v0.x, std::min(v1.x, v2.x));
    minY = std::min(v0.y, std::min(v1.y, v2.y));
    minZ = std::min(v0.z, std::min(v1.z, v2.z));

    return Vector3(minX, minY, minZ);
}

Vector3
Triangle::getMaxPoint()
{
        TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
    
    const Vector3 & v0 = m_mesh->vertices()[ti3.x]; // vertex a of triangle
    const Vector3 & v1 = m_mesh->vertices()[ti3.y]; // vertex b of triangle
    const Vector3 & v2 = m_mesh->vertices()[ti3.z]; // vertex c of triangle

    float maxX = 0, maxY = 0, maxZ = 0;
    
    maxX = std::max(v0.x, std::max(v1.x, v2.x));
    maxY = std::max(v0.y, std::max(v1.y, v2.y));
    maxZ = std::max(v0.z, std::max(v1.z, v2.z));

    return Vector3(maxX, maxY, maxZ);
}

#define EPSILON 0.00000024f
bool
Triangle::intersect(HitInfo& result, const Ray& r,float tMin, float tMax)
{
    TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
    const Vector3 & v0 = m_mesh->vertices()[ti3.x]; // vertex a of triangle
    const Vector3 & v1 = m_mesh->vertices()[ti3.y]; // vertex b of triangle
    const Vector3 & v2 = m_mesh->vertices()[ti3.z]; // vertex c of triangle

    TriangleMesh::TupleI3 ni3 = m_mesh->nIndices()[m_index];
    const Vector3 & n0 = m_mesh->normals()[ni3.x];
    const Vector3 & n1 = m_mesh->normals()[ni3.y];
    const Vector3 & n2 = m_mesh->normals()[ni3.z];

    // simple solution based on Cramer's rule
    Vector3 e1, e2, right;

    e1 = v0 - v1;
    e2 = v0 - v2;

    Vector3 eCross = cross(e1, e2);
    
    float det = dot(eCross, r.d);
    if (det > -EPSILON && det < EPSILON)
	return false;
    float div = 1.0 / det;

    right = v0 - r.o;
    float beta = dot(cross(right, e2), r.d);
    beta = beta * div;

    if (beta < 0.0 || beta > 1.0)
	return false;

    float gamma = dot(cross(e1, right), r.d);
    gamma = gamma * div;

    if (gamma < 0.0 || gamma > 1.0)
	return false;
    
    float alpha = 1.0 - beta - gamma;
    if (alpha > 1.0 || alpha < 0.0)
	return false;

    float t = dot(eCross, right);
    t = t * div;

    if (t < tMin || t > tMax)
	return false;

    result.t = t;
    result.P = r.o + result.t*r.d;

    Vector3 normal = alpha*n0 + beta*n1 + gamma*n2; // interpolated shading
    result.N = normal;
    result.N.normalize();
    result.material = m_material;

    return true;
}

// bool
// Triangle::intersect(HitInfo& result, const Ray& r,float tMin, float tMax)
// {
//     TriangleMesh::TupleI3 ti3 = m_mesh->vIndices()[m_index];
//     const Vector3 & v0 = m_mesh->vertices()[ti3.x]; // vertex a of triangle
//     const Vector3 & v1 = m_mesh->vertices()[ti3.y]; // vertex b of triangle
//     const Vector3 & v2 = m_mesh->vertices()[ti3.z]; // vertex c of triangle

//     Vector3 normal = m_mesh->normals()[m_index];

//     Vector3 h, e1, e2, s, q;
//     float det, f, u, v, t;

//     e1 = v1 - v0;
//     e2 = v2 - v0;

//     h = cross(r.d, e2);
//     det = dot(e1, h);

//     if (det > -EPSILON && det < EPSILON)
// 	return false;
    
//     f = 1/det;
//     s = r.o - v0;
//     u = f * (dot(s, h)); // one barycentric coord

//     if (u < 0.0 || u > 1.0)
// 	return false;

//     q = cross(s, e1);
//     v = f * dot(r.d, q); // the other barycentric coord

//     if (v < 0.0 || v > 1.0)
// 	return false;

//     // compute t
//     t = f * dot(e2, q);
//     if (t < EPSILON || t < tMin || t > tMax){
// 	return false;
//     }

//     result.t = t;
//     result.P = r.o + result.t*r.d;
//     result.N = normal;
//     result.N.normalize();
//     result.material = this->m_material;

//     return true;
// }
