#ifndef CSE168_BVH_H_INCLUDED
#define CSE168_BVH_H_INCLUDED

#include "Miro.h"
#include "Object.h"
#include "BVHNode.h"

class BVH
{
public:
    ~BVH();
    void build(Objects * objs);

    void drawGL();

    bool intersect(HitInfo& result, const Ray& ray,
		   float tMin = 0.0f, float tMax = MIRO_TMAX) const;


protected:
    Objects * m_objects;

private:
    BVHNode* m_root;

    static bool sortFunction(Object* o1, Object* o2);
    
    void calculateBoundingBox(const Objects& objects, int start, int end,
			      Vector3& pMin, Vector3& pMax);
    
    int calculateBestSplit(const BVHNode& node);
    
    float calculateBoxVolume(const Objects& objects, int start, int end);

    void drawBox(const Vector3& color, const Vector3& pMin, const Vector3& pMax); 
};



#endif // CSE168_BVH_H_INCLUDED
