#include <cmath>

#include "Refract.h"
#include "Scene.h"
#include "Ray.h"
#include "HashTable.h"

Refract::Refract(float refractionIndex, const Vector3& ks):
    m_index(refractionIndex),
    m_ks(ks)
{

}

void
Refract::preCalc()
{
    // Nothing to do, for now.
}

bool
Refract::computeNormal(const Ray& ray, const HitInfo& hit, Vector3* normal) const
{
    bool goingIn;
    if (dot(ray.d, hit.N) > 0.0f)
    {
	*normal = -hit.N;
	goingIn = false;
    }
    else
    {
	*normal = hit.N;
	goingIn = true;
    }
    return goingIn;
}

float
Refract::computeSqrtPart(float indexRatio, float rayNormalDot) const
{
    return 1 - (indexRatio*indexRatio)*(1 - (rayNormalDot*rayNormalDot));
}

Ray
Refract::getRefractedRay(const Ray& ray, const HitInfo& hit, const Vector3& normal, float indexRatio,
			 float rayNormalDot, float sqrtPart) const
{
    Vector3 wt = indexRatio * (ray.d - rayNormalDot * normal) - sqrt(sqrtPart) * normal;
 
    Ray refractedRay = ray;
    refractedRay.o = hit.P;
    refractedRay.d = wt.normalize();
    refractedRay.depth = ray.depth + 1;

    return refractedRay;
}


Ray
Refract::getReflectedRay(const Ray& ray, const HitInfo& hit) const
{
    Vector3 reflectedDir = -2 * dot(ray.d, hit.N) * hit.N + ray.d;
    reflectedDir.normalize();
    Ray reflectedRay = ray;
    reflectedRay.o = hit.P;
    reflectedRay.d = reflectedDir;
    reflectedRay.depth = ray.depth + 1;
    return reflectedRay;
}

float
Refract::computeFresnelTerm(float n1, float n2,
			    const Vector3& normal, const Vector3& wi, const Vector3& wt) const
{
    float cosi = dot(-wi, normal);
    float coso = dot(wt, -normal);
    float ps = (n1*cosi - n2*coso)/(n1*cosi + n2*coso);
    float pt = (n1*coso - n2*cosi)/(n1*coso + n2*cosi);

    float re = 0.5*(ps*ps + pt*pt);
    return re;
}

Vector3
Refract::shade(const Ray& ray, const HitInfo& hit,
	       const Scene& scene) const
{
    // if (ray.depth > 20)
    // 	return m_ks;
    
    // Ray refractedRay = getRefractedRay(ray, hit);
    // HitInfo refractedHit;
    // if (scene.trace(refractedHit, refractedRay, 0.0024f))
    // {
    // 	Vector3 result = refractedHit.material->shade(refractedRay, refractedHit, scene);
    // 	return m_ks * result;
    // }
    
    return m_ks * Vector3(0.0f);
}

void
Refract::trace(std::vector<MeasurementPoint*>& mPoints,
	       const Ray& ray, HitInfo& hit,
	       const Scene& scene,
	       const Vector3& adj,
	       float tMin, float tMax) const
{
    if (ray.depth > 20)
    	return;
    
    Ray reflectedRay = getReflectedRay(ray, hit);
    Ray refractedRay = ray;

    Vector3 normal(0.0f);
    bool goingIn = computeNormal(ray, hit, &normal);
    if (!goingIn)
    	refractedRay.pop();

    float indexRatio = refractedRay.head() / m_index;
    
    float rayNormalDot = dot(refractedRay.d, normal);
    

    float sqrtPart = computeSqrtPart(indexRatio, rayNormalDot);

    if (sqrtPart < 0.0f) // total internal reflection
    {
    	scene.traceEyeRay(mPoints, reflectedRay, adj, 0.0024f);
    	return;
    }
    
    refractedRay = getRefractedRay(refractedRay, hit, normal, indexRatio, rayNormalDot, sqrtPart);

    float re = computeFresnelTerm(refractedRay.head(), m_index, normal, ray.d, refractedRay.d);
    
    if (goingIn)
    	refractedRay.push(m_index);
    
    Vector3 newAdj = m_ks*adj;
    scene.traceEyeRay(mPoints, reflectedRay, re*newAdj, 0.000024f);
    scene.traceEyeRay(mPoints, refractedRay, (1-re)*newAdj, 0.000024f);
}

void
Refract::photonShade(HashTable& mpTable, Ray& ray, Scene& scene,
		     HitInfo& hit, Vector3& flux, Vector3& adj) const
{
    if (ray.depth > 20 || flux.length() < 0.001f)
	return;

    Ray reflectedRay = getReflectedRay(ray, hit);

    Vector3 normal(0.0f);
    bool goingIn = computeNormal(ray, hit, &normal);
    if (!goingIn)
	ray.pop();
    
    float indexRatio = ray.head() / m_index;
    float rayNormalDot = dot(ray.d, normal);

    float sqrtPart = computeSqrtPart(indexRatio, rayNormalDot);

    if (sqrtPart < 0.0f) // total internal reflection
    {
	scene.tracePhotonRay(reflectedRay, flux, adj, 0.0024f);
	return;
    }
    
    Ray refractedRay = getRefractedRay(ray, hit, normal, indexRatio, rayNormalDot, sqrtPart);

    float re = computeFresnelTerm(refractedRay.head(), m_index, normal, ray.d, refractedRay.d);
    
    if (goingIn)
	refractedRay.push(m_index);

    float r1 = static_cast<float>(rand())/static_cast<float>(RAND_MAX);
    Vector3 newAdj = m_ks*adj;
    if (r1 < re)
    {
	scene.tracePhotonRay(reflectedRay, flux, newAdj, 0.0024f);
    }
    else
    {
	scene.tracePhotonRay(refractedRay, flux, newAdj, 0.0024f);	
    }
}

