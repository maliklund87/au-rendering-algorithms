#ifndef MPTREE_H_INCLUDED
#define MPTREE_H_INCLUDED

#include <vector>
#include "Vector3.h"
#include "HashTable.h"

class MeasurementPoint;

class MPTree : public HashTable
{
    typedef std::vector<MeasurementPoint *> Bucket;
public:
    MPTree(const Vector3& minPoint, const Vector3& maxPoint, float radius, int childSize = 2);
    ~MPTree();

    inline void add(MeasurementPoint* mp) override
    {
	std::cout << std::endl << mp->pos << std::endl;
	Vector3 minPoint = ((mp->pos - pointRadius) * incDiv) + constant;
	Vector3 maxPoint = ((mp->pos + pointRadius) * incDiv) + constant;
	for (float iz = minPoint.z; iz < maxPoint.z; iz += inc.z)
	{
	    std::cout << iz << std::endl;
	    for (float iy = minPoint.y; iy < maxPoint.y; iy += inc.y)
	    {
		std::cout << " >" << iy << std::endl;
		for (float ix = minPoint.x; ix < maxPoint.x; ix += inc.x)
		{
		    std::cout << "  >" << ix;
		    int index = convert(iz, iy, ix);
		    std::cout << " ==> " << index << std::endl;
		    mpLists[index]->push_back(mp);
		}
	    }
	}
	
    }

    virtual inline std::vector<MeasurementPoint*>& get(float x, float y, float z) override
    {
	return get(Vector3(x, y, z));
    }

    inline std::vector<MeasurementPoint*>& get(const Vector3& p)
    {
	return *(mpLists[getIndex(p)]);
    }

    inline std::vector<std::vector<MeasurementPoint*>* >& getHashList() {
	return dummyList;
    }

private:
    int size; // the nr of children pr node.
    int size2;
    int size3;
    int nrOfElements; // the total nr of children.
    float pointRadius;
    Vector3 min, max, offset, inc, incDiv, constant;

    std::vector<Bucket*> mpLists;
    
    std::vector<std::vector<MeasurementPoint*>* > dummyList;

    inline int convert(const Vector3& i) const
    {
	return convert(i.z, i.y, i.x);
    }

    inline int convert(float z, float y, float x) const
    {
	int result = z*size + y*size2 + x*size3;
	return result < 0 || result > nrOfElements ? 0 : result;
    }

    inline int getIndex(const Vector3& p)
    {
	//Vector3 i = ((p + offset) - min) * inc;
	Vector3 i = p*incDiv + constant; // == ((p + offset) - min) * inc
	return convert(i);
    }
};

#endif
