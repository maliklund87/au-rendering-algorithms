#include "Timer.h"


void
Timer::start()
{
    gettimeofday(&_start, 0);
}

void
Timer::stop()
{
    gettimeofday(&_end, 0);
}

double
Timer::getElapsedTime()
{
    double elapsedTime;
    elapsedTime = (_end.tv_sec - _start.tv_sec) * 1000.0;      // sec to ms
    elapsedTime += (_end.tv_usec - _start.tv_usec) / 1000.0;   // us to ms
    return elapsedTime;
}
