#ifndef CSE168_SCENE_H_INCLUDED
#define CSE168_SCENE_H_INCLUDED

#include <memory>
#include <string>

#include "Miro.h"
#include "Object.h"
#include "PointLight.h"
#include "BVH.h"
#include "PFMImage.h"


class Camera;
class Image;
class MeasurementPoint;
class HashTable;
class MPTree;

class Scene
{
public:
    Scene();
    ~Scene();
    
    void addObject(Object* pObj)        {m_objects.push_back(pObj);}
    Objects* objects()      {return &m_objects;}

    void addLight(PointLight* pObj)     {m_lights.push_back(pObj);}
    const Lights* lights() const        {return &m_lights;}

    void setEnvMap(std::string path)
    {
	m_envMap = new PFMImage(path);
	m_envMapEnabled = true;
    }
    
    inline bool isEnvMapEnabled() { return m_envMapEnabled; }

    Vector3 getEnvMapColor(const Vector3& dir);

    void preCalc();
    void openGL(Camera *cam);

    void updateImage(Image& img, int j);
    
    void mapMeasurementPoints();

    void raytraceImage(Camera *cam, Image *img);

    void traceEyeRay(std::vector<MeasurementPoint*>& hitPoints, const Ray& ray,
		     const Vector3& adj,
		     float tMin = 0.0f, float tMax = MIRO_TMAX) const;
    
    void tracePhotonRay(Ray& ray, Vector3& flux, Vector3& adj,
			float tMin = 0, float tMax = MIRO_TMAX);
    bool trace(HitInfo& minHit, const Ray& ray,
               float tMin = 0.0f, float tMax = MIRO_TMAX) const;

protected:
    Objects m_objects;
    BVH m_bvh;
    Lights m_lights;

    bool m_envMapEnabled;
    PFMImage* m_envMap;

    bool antiAliasingEnabled;

    HashTable* mpHashTable;
    //MPTree* mpHashTable;

    // Photon density mapping
    unsigned int nrOfMPoints;
    std::vector<MeasurementPoint*> mPoints;
    unsigned int nrOfPhotons;
    int raysPrPixel;

    MeasurementPoint* createMeasurementPoint(HitInfo* hit, int pixelPosition);
    void buildHashGrid(int width, int height);
    void tracePhotonRays();
    inline void adjustBoundingBox(const Vector3& pos, Vector3* bbMin, Vector3* bbMax);
};

extern Scene * g_scene;

#endif // CSE168_SCENE_H_INCLUDED
